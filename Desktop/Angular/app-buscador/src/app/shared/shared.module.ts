//MODULOS IMPORTADOS
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//COMPONENTES IMPORTADOS
import { CardComponent } from './card/card.component';
import { HeaderComponent } from './header/header.component';
import { FilterComponent } from './filter/filter.component';

@NgModule({
  declarations: [
    CardComponent,
    HeaderComponent,
    FilterComponent
  ],
  exports: [
    CardComponent,
    HeaderComponent,
    FilterComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
